<div class="panel panel-default" style="margin-bottom:70px;">
    <div class="panel-body">
        <h3>Artists</h3>
        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
                <th>Artist Name</th>
                <th>Albums</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </thead>

            <!-- Table Body -->
            <tbody>
                @foreach ($artists as $artist)
                    <tr>
                        <!-- Artist Name -->
                        <td class="table-text">
                            <div>{{ $artist->name }}</div>
                        </td>

                        <td>
                            @if (count($artist->albums) > 0)
                               {{ implode(', ', array_map(function($obj) {return $obj['name'];}, $artist->albums->toArray())) }}
                            @endif
                        </td>

                        <td>
                            <a href="{{ route('artist_edit', ['artist' => $artist->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                        </td>

                        <td>
                            <!-- Delete Button -->
                            <form action="{{ route('artist_delete', ['artist' => $artist->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <input type="submit" class="btn btn-danger btn-sm" value="Delete"/>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>