@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-4">
        <h3>Edit artist</h3>

        @include('common.errors')

        <form action="{{ route('artist_edit', ['artist' => $artist->id]) }}" method="POST">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $artist->name) }}">
            </div>
            <input type="submit" class="btn btn-success" value="Save"/> 
            <a href="{{ route('home') }}" class="btn btn-warning">Back</a>
        </form>
    </div>
</div>
@endsection