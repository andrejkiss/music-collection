@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="panel-body">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <div class="card">
                <div class="card-header">
                    New Artist
                </div>
                <div class="card-body">
                    <!-- New Artist Form -->
                    <form action="{{ route('artist_create') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Artist Name -->
                        <div class="form-group">
                            <label for="artist-name" class="col-sm-3 control-label">Artist name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="artist-name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>

                        <!-- Add Artist Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Add Artist
                                </button>
                                <a href="{{ route('home') }}" class="btn btn-warning">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection