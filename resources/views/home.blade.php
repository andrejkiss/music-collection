@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        
    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('artist_create') }}">Add artist</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('album_create') }}">Add album</a>
        </li>
    </ul>

        @if (count($artists) > 0)
            @include('artists.artists_table')
        @endif
        
        @if (count($albums) > 0)
            @include('albums.albums_table')
        @endif
    </div>
</div>

@endsection