@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="panel-body">
            <!-- Display Validation Errors -->
            @include('common.errors')

            <div class="card">
                <div class="card-header">
                    New Album
                </div>
                <div class="card-body">
                    <!-- New Album Form -->
                    <form action="{{ route('album_create') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Album Name -->
                        <div class="form-group">
                            <label for="album-name" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="album-name" class="form-control" value="{{ old('name') }}">
                            </div>
                        </div>

                        <!-- Album artist -->
                        <div class="form-group">
                            <label for="album-artist" class="col-sm-3 control-label">Artist</label>

                            <div class="col-sm-6">
                                <select class="form-control" name="artist_id">
                                    <option value="0">Select Artist</option>
                                    @foreach ($artists as $artist)
                                        <option value="{{ $artist->id}}" {{ $artist->id == old('artist_id') ? 'selected' : ''  }}>{{ $artist->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <!-- Add Album Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-plus"></i> Add Album
                                </button>
                                <a href="{{ route('home') }}" class="btn btn-warning">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection