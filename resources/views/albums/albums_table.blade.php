<div class="panel panel-default">
    <div class="panel-body">
        <h3>Albums</h3>
        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
                <th>Album Name</th>
                <th>Artist</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </thead>

            <!-- Table Body -->
            <tbody>
                @foreach ($albums as $album)
                    <tr>
                        <!-- Album Name -->
                        <td class="table-text">
                            <div>{{ $album->name }}</div>
                        </td>

                        <td>
                            <div>{{ $album->artist->name }}</div>
                        </td>

                        <td>
                            <a href="{{ route('album_edit', ['album' => $album->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                        </td>

                        <td>
                            <!-- Delete Button -->
                            <form action="{{ route('album_delete', ['album' => $album->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <input type="submit" class="btn btn-danger btn-sm" value="Delete"/>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>