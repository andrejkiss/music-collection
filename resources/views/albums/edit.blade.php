@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-4">
        <h3>Edit album</h3>

        @include('common.errors')

        <form action="{{ route('album_edit', ['album' => $album->id]) }}" method="POST">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="album-name">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $album->name) }}">
            </div>

            <div class="form-group">
                <label for="album-artist" class="col-sm-3 control-label">Artist</label>
                    <select class="form-control" name="artist_id">
                        @foreach ($artists as $artist)
                            <option value="{{ $artist->id}}" {{ $artist->id == old('artist_id', $album->artist_id) ? 'selected' : ''  }}>{{ $artist->name }}</option>
                        @endforeach
                    </select>
            </div>

            <input type="submit" class="btn btn-success" value="Save"/> 
            <a href="{{ route('home') }}" class="btn btn-warning">Back</a>
        </form>
    </div>
</div>
@endsection