@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        
    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('artist_create') }}">Add artist</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('album_create') }}">Add album</a>
        </li>
    </ul>

        @if (count($artists) > 0)
            <div class="panel panel-default">

                <div class="panel-body">
                    <table class="table table-striped task-table">

                        <!-- Table Headings -->
                        <thead>
                            <th>Artists list</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </thead>

                        <!-- Table Body -->
                        <tbody>
                            @foreach ($artists as $artist)
                                <tr>
                                    <!-- Artist Name -->
                                    <td class="table-text">
                                        <div>{{ $artist->name }}</div>
                                    </td>

                                    <td>
                                        <a href="{{ route('artist_edit', ['artist' => $artist->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                    </td>

                                    <td>
                                        <!-- Delete Button -->
                                        <form action="{{ route('artist_delete', ['artist' => $artist->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <input type="submit" class="btn btn-danger btn-sm" value="Delete"/>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
</div>

@endsection