<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::match(['get', 'post'], '/artist/{artist}', 'ArtistController@edit')->name('artist_edit');
Route::match(['get', 'post'], '/artist_create', 'ArtistController@create')->name('artist_create');
Route::delete('/artist/{artist}', 'ArtistController@delete')->name('artist_delete');

Route::match(['get', 'post'], '/album_create', 'AlbumController@create')->name('album_create');
Route::match(['get', 'post'], '/album/{album}', 'AlbumController@edit')->name('album_edit');
Route::delete('/album/{album}', 'AlbumController@delete')->name('album_delete');