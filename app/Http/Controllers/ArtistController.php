<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;

class ArtistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(Request $request, Artist $artist)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required|max:255'
            ]);

            $artist->name = $request->name;
            $artist->save();
            return redirect('home');
        }

        return view('artists.edit', [
            'artist' => $artist
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required|max:255'
            ]);

            $artist = Artist::create($request->all());

            return redirect('home');
        }

        return view('artists.create');
    }

    public function delete(Request $request, Artist $artist)
    {
        if ($artist->albums) {
            foreach ($artist->albums as $album) {
                $album->delete();
            }
        }

        $artist->delete();
        return redirect('home');
    }
}
