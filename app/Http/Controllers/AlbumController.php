<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artist;
use App\Album;

class AlbumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required|max:255',
                'artist_id' => 'required|not_in:0'
            ]);

            $album = Album::create($request->all());

            return redirect('home');
        }

        return view('albums.create', [
            'artists' => Artist::all()
        ]);
    }

    public function edit(Request $request, Album $album)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required|max:255',
                'artist_id' => 'required|not_in:0'
            ]);

            $album->name = $request->name;
            $album->artist_id = $request->artist_id;
            $album->save();
            return redirect('home');
        }

        return view('albums.edit', [
            'album' => $album,
            'artists' => Artist::all()
        ]);
    }

    public function delete(Request $request, Album $album)
    {
        $album->delete();
        return redirect('home');
    }
}
